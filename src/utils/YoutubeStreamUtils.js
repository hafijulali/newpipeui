import { ifElse, head, last, path, pipe, sortBy, prop } from 'ramda';
import store from '../store/store'

export const getBestAudioStream = (audioStreams) => {
  const opusStreams = audioStreams.filter(it => it.format == 'WEBMA_OPUS');
  return opusStreams.find(it => it.averageBitrate > 100) || opusStreams[0] || audioStreams[0];
};

const slowSpeedInternet = () => store.state.prefs.slowSpeedInternet

export const getBestFrames = pipe(sortBy(path('frameWidth')), last)

export const getThumbnailUrl = pipe(sortBy(path('width')), ifElse(slowSpeedInternet, head, last), prop('url'))
