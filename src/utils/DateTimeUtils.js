import { differenceInMinutes } from 'date-fns';
import intervalToDuration  from 'date-fns/intervalToDuration';

export const doubleDigit = (number) => {
  return ("0" + number).slice(-2);
}
export const formatDuration = (seconds) => {
  const duration = intervalToDuration({ start: 0, end: seconds * 1000 })
  return duration.hours > 0 ? `${doubleDigit(duration.hours)}:${doubleDigit(duration.minutes)}:${doubleDigit(duration.seconds)}`
    : `${doubleDigit(duration.minutes)}:${doubleDigit(duration.seconds)}`
}

export const inLastMinute = (timestamp) => {
  return differenceInMinutes(timestamp, new Date()) <= 1;
}
