import { isEmpty } from 'ramda';

const computeBasePath = () => {
  const path = window.location.pathname.substring(0, window.location.pathname.indexOf("/route"));
  return isEmpty(path) ? "/" : path;
};

const urlContainsAppPath = window.location.pathname.includes("route");
export const base = urlContainsAppPath ? computeBasePath() : window.location.pathname;

export const openInNewTab = (routeInfo, router) => {
  let routeData = router.resolve(routeInfo);
  window.open(routeData.href, '_blank');
}
