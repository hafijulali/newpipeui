import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Stream from '../views/Stream.vue'
import Channel from '../views/Channel.vue'
import { base } from '../utils/RouterUtils'
import Search from '../views/Search.vue'

Vue.use(VueRouter)


const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    default: true,
  },
  {
    path: '/routestream',
    name: 'Stream',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: () => import(/* webpackChunkName: "about" */ '../views/Stream.vue'),
    component: Stream,
    props: true
  },
  {
    path: '/routesearch',
    name: 'Search',
    component: Search,
  },
  {
    path: '/routechannel',
    name: 'Channel',
    component: Channel,
  }
]

const router = new VueRouter({
  mode: 'history',
  base,
  routes,
  fallback: 'true'
})

export default router
