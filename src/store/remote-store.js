const updateLocalStorage = (newState) => {
  Object.keys(newState)
    .forEach(key => localStorage[key] = newState[key])
}

export default {
  namespaced: true,
  mutations: {
    syncToLocal: (_, remoteState) => {
      const localStateAsString = JSON.stringify(localStorage)
      const localState = JSON.parse(localStateAsString)
      if(JSON.stringify(remoteState) == localStateAsString) return localStateAsString;
      updateLocalStorage(Object.assign(remoteState, localState))
    }
  },
  getters: {
    localState: (_) => () => {
      return JSON.stringify(localStorage)
    }
  }
};


