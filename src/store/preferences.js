import { addHours } from 'date-fns';

const wasteTimeCacheKey = 'wasteTime';
const defaultResolutionCacheKey = 'defaultResolution';
const preferredVolumeKey = 'preferredVolume';
const showCaptionsKey = 'showCaptions';
const onlyAudioKey = 'onlyAudio';
const slowSpeedInternetKey = 'slowInternet';

const wasteTimeValidityCacheKey = 'wasteTimeValidity'
const wasteTimeValidity = Number.parseInt(window.localStorage.getItem(wasteTimeValidityCacheKey) || 0);
const isWasteTimeCacheValid = new Date().getTime() < wasteTimeValidity;

export const getStringFromPref = key => window.localStorage.getItem(key);
export const getBooleanFromPref = key => 'true' === window.localStorage.getItem(key);
const getNumberFromPref = key => Number.parseFloat(window.localStorage.getItem(key) || 0);

export default {
  namespaced: true,
  state: {
    wasteTime: isWasteTimeCacheValid ? getBooleanFromPref(wasteTimeCacheKey) : false,
    defaultResolution: getStringFromPref(defaultResolutionCacheKey) || "360p",
    preferredVolume: getNumberFromPref(preferredVolumeKey) || 0.5,
    onlyAudio: getBooleanFromPref(onlyAudioKey),
    showCaptions: getBooleanFromPref(showCaptionsKey),
    slowSpeedInternet: getBooleanFromPref(slowSpeedInternetKey) || false
  },
  mutations: {
    toggleWasteTime: state => {
      state.wasteTime = !state.wasteTime;
      window.localStorage.setItem(wasteTimeCacheKey, state.wasteTime)
      window.localStorage.setItem(wasteTimeValidityCacheKey, addHours(new Date(), 1).getTime().toString())
    },
    toggleOnlyAudio: state => {
      state.onlyAudio = !state.onlyAudio;
      window.localStorage.setItem(onlyAudioKey, state.onlyAudio)
    },
    toggleShowCaptions: state => {
      state.showCaptions = !state.showCaptions;
      window.localStorage.setItem(showCaptionsKey, state.showCaptions)
    },
    setDefaultResolution : (state, resolutionName) => {
      state.defaultResolution = resolutionName;
      window.localStorage.setItem(defaultResolutionCacheKey, state.defaultResolution)
    },
    setPreferredVolume : (state, volume) => {
      state.preferredVolume = volume;
      window.localStorage.setItem(preferredVolumeKey, state.preferredVolume)
    },
    setIsSlowSpeedInterent: (state, isSlow) => {
      state.slowSpeedInternet = isSlow;
      window.localStorage.setItem(slowSpeedInternetKey, state.slowSpeedInternet)
    }
  }
};
