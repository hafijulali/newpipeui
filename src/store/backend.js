import { getStringFromPref, getBooleanFromPref } from './preferences';
const defaultInstance = "https://newpipe-service.onrender.com";
const localhostInstance = "http://localhost:6060"
const backendUrlCacheKey = 'backendUrl';
const shouldUseLocalhostAsBackendKey = 'shouldUseLocalhostAsBackend'

const queryUrl = backendUrl => backendUrl + `/query`
const streamDetailsUrl = (weburl, backendUrl) => backendUrl + `/stream?url=${weburl}`
const channelStreamsFetchUrl = (weburl, backendUrl) => backendUrl + `/channel?url=${weburl}`
const feedURL = backendUrl => backendUrl + `/feed`
const proxyUrl = (weburl, backendUrl) => backendUrl + `/proxy?url=${encodeURIComponent(weburl)}`
const remoteStateUrl = backendUrl => backendUrl + `/store`

const postRequest = data => {
  const dataToSend = typeof data === "object" ? JSON.stringify(data) : data;
  return {
    method: 'POST', 
    body: dataToSend
  };
}

const fetchThatFails = (url, options) =>
  fetch(url, options)
  .then(resp => {
    if (resp.ok) return resp;
    else throw resp;
  });

const retry = (times, functionality) => {
  return Promise.resolve()
    .then(functionality)
    .catch(err => {
      console.log('retrying...', err);
      if (times == 1) return Promise.reject(err);
      return retry(times -1, functionality);
    });
};

const shouldUseLocalhostAsBackend = getBooleanFromPref(shouldUseLocalhostAsBackendKey)
const backendUrl = getStringFromPref(backendUrlCacheKey) || defaultInstance;

const getServerUrl = (state) => state.shouldUseLocalhostAsBackend ? localhostInstance : state.backendUrl;

export default {
  namespaced: true,
  state: {
    backendUrl: backendUrl,
    shouldUseLocalhostAsBackend: shouldUseLocalhostAsBackend
  },
  mutations: {
    setBackendUrl: (state, backendUrl) => {
      state.backendUrl = backendUrl;
      window.localStorage.setItem(backendUrlCacheKey, state.backendUrl)
    },
    toggleShouldUseLocalhostAsBackend: (state) => {
      state.shouldUseLocalhostAsBackend = !state.shouldUseLocalhostAsBackend;
      window.localStorage.setItem(shouldUseLocalhostAsBackendKey, state.shouldUseLocalhostAsBackend);
    }
  },
  getters: {
    query: state => searchString => {
      return fetchThatFails(queryUrl(getServerUrl(state)), postRequest(searchString))
        .then(resp => resp.json())
    },
    fetchVideosOfChannel: state => (weburl) => {
      return fetchThatFails(channelStreamsFetchUrl(weburl, getServerUrl(state)))
        .then(resp => resp.json())
    },
    fetchStreamDetails: state => (weburl) => {
      return retry(3, () => fetchThatFails(streamDetailsUrl(weburl, getServerUrl(state))))
        .then(resp => resp.json())
    },
    proxy: state => (url) => {
      return fetchThatFails(proxyUrl(url, getServerUrl(state)))
        .then(resp => resp.blob())
        .then(blob => URL.createObjectURL(blob));
    },
    feed: state => () => {
      return fetchThatFails(feedURL(getServerUrl(state)))
        .then(resp => resp.json())
    },
    remoteState: state => () => {
      return fetchThatFails(remoteStateUrl(getServerUrl(state)))
        .then(resp => resp.json())
    },
    generateProxyUrl: state => (url) => proxyUrl(url, getServerUrl(state)),
    uploadState: state => newStateAsString => {
      return fetchThatFails(remoteStateUrl(getServerUrl(state)), postRequest(newStateAsString))
    }
  }
};

