import { getStringFromPref, getBooleanFromPref } from './preferences';
const playlistPrefKey = 'playlist';
const repeatPlaylistKey = 'repeatPlaylist';

const getPlaylistFromStorage = () => JSON.parse(getStringFromPref(playlistPrefKey) || "[]");
const savePlaylist = playlist => window.localStorage.setItem(playlistPrefKey, JSON.stringify(playlist));

export default {
  namespaced: true,
  state: {
    playlist: [],
    currentPlaying: {},
    repeatPlaylist: getBooleanFromPref(repeatPlaylistKey),
  },
  mutations: {
    addTrack: (state, {url, name}) => {
      const playlist = getPlaylistFromStorage();
      playlist.push({url, name});
      state.playlist = playlist;
      savePlaylist(playlist);
    },
    removeTrack: (state, track) => {
      const playlist = getPlaylistFromStorage().filter(item => item.url != track.url);
      state.playlist = playlist;
      savePlaylist(playlist);
    },
    toggleRepeatPlaylist : state => {
      state.repeatPlaylist = !state.repeatPlaylist;
      window.localStorage.setItem(repeatPlaylistKey, state.repeatPlaylist);
    },
    clear: (state)  => {
      state.playlist = [];
      state.currentPlaying = {};
      savePlaylist(state.playlist);
    },
    setCurrentPlaying: (state, {url, name}) => {
      state.currentPlaying = {url, name};
    }
  },
  getters: {
    getLatestTrackList: (state) => () => {
      state.playlist = getPlaylistFromStorage();
      return state.playlist;
    },
    getNextTrack: state => () => {
      const playlist = getPlaylistFromStorage();
      const currentPlayingIndex = playlist.findIndex(it => it.url == state.currentPlaying.url); 
      if ((currentPlayingIndex + 1 == playlist.length) && state.repeatPlaylist) return playlist[0] || {};
      return playlist[currentPlayingIndex + 1] || {};
    }
  }
};


