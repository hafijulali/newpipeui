import { getStringFromPref } from './preferences';
const subscriptionsPrefKey = 'subscriptions';

const getSubscriptionsFromStorage = () => JSON.parse(getStringFromPref(subscriptionsPrefKey) || "[]");
const saveSubscriptions = subscriptions => window.localStorage.setItem(subscriptionsPrefKey, JSON.stringify(subscriptions));

export default {
  namespaced: true,
  state: {
    subscriptions: getSubscriptionsFromStorage(),
  },
  mutations: {
    addSubscription: (state, subscription) => {
      const subscriptions = getSubscriptionsFromStorage();
      subscriptions.push(subscription);
      state.subscriptions = subscriptions;
      saveSubscriptions(subscriptions);
    },
    removeSubscription: (state, subscription) => {
      const subscriptions = getSubscriptionsFromStorage().filter(item => item.url != subscription.url);
      state.subscriptions = subscriptions;
      saveSubscriptions(subscriptions);
    },
  },
};


