# base image to build from
FROM node:current-slim AS buildStage

# setting /app of container as work dir
WORKDIR /app

# copying contents of ui code into /app [into container]
COPY . /app/

# installing dependencies
RUN npm install

# generating production build
RUN npm run build

# serving the app using nginx
FROM nginxinc/nginx-unprivileged:alpine-slim

COPY --from=buildStage /app/dist /usr/share/nginx/html
